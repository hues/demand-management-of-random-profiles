# README #

Electrical appliances do not have constant electricity consumption patterns. This method enables the demand manager to shift any consumption pattern in time using MILP formulations.

Version 0.1

Requires MATLAB, YALMIP and a MILP solver