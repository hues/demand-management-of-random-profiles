clear variables;
close all;
window=10;
grid=sdpvar(1,window);
%price=[3 3 3 3 3 3 3 2 2 3];
price=[3 3 3 3 3 3 2 3 3 3];
T=3;
Lp=[2 1 3];

onoff=binvar(1,window);
load=sdpvar(1,window);
Constraints=grid==load;
Constraints=[Constraints,sum(onoff)==T,load>=0,onoff(1)==0];
for k = 2:window
    
    % indicator will be 1 only when switched on
    indicator(k) = onoff(k)-onoff(k-1);
    range = k:min(window,k+T-1);
    % Constraints will be redundant unless indicator = 1
    Constraints = [Constraints, onoff(range) >= indicator(k)];
    Constraints = [Constraints, load(range) >= Lp(1:length(range))*indicator(k)];
end

Objective = 0;
for k = 1:window
    Objective = Objective + price(k).*grid(k);
end
optimize(Constraints,Objective);
figure;
%plot(1:window,-value(grid),1:window,value(onoff),1:window,price,1:window,value(load),1:window,value(indicator));
stairs(1:window,[-value(grid);value(onoff);price;value(load);value(indicator)]');
legend('Grid','OnOff','Price','Load','Indicator');
%xlabel('hours'); ylabel('load');